import Box from '@mui/material/Box';

const Spacer = () => {
	return <Box sx={{ flexGrow: 1 }} />;
};

export default Spacer;
