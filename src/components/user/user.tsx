import { Grid, Stack, Typography } from '@mui/material';
import { FC } from 'react';
import { TUser } from '../../models/models';

type TUserProps = {
	user: TUser | null;
};

const User: FC<TUserProps> = ({ user }) => {
	return (
		<>
			{user && (
				<Grid container spacing={2}>
					<Grid item xs={12} lg={6}>
						<img width={'100%'} src={user.avatar} alt='TODO' loading='lazy' />
					</Grid>
					<Grid item xs={12} lg={6}>
						<Stack direction={'column'} spacing={2}>
							<Typography variant='h4'>{user.name}</Typography>
							<Typography variant='body1'>{user.email}</Typography>
							<Typography variant='body1'>{user.group}</Typography>
							<Typography variant='body1'>{user.about}</Typography>
						</Stack>
					</Grid>
				</Grid>
			)}
		</>
	);
};

export default User;
