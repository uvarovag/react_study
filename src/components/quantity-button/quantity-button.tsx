import { FC } from 'react';
import { IconButton, Stack, SxProps, Typography } from '@mui/material';
import { Add, AddShoppingCart, Remove } from '@mui/icons-material';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { selectProductCount } from '../../store/reducers/cart/selectors';
import { productDec, productInc } from '../../store/reducers/cart/cart-slice';

type TQuantityButtonProps = {
	productId: string;
	stock: number;
	sx?: SxProps;
};

const QuantityButton: FC<TQuantityButtonProps> = ({ productId, stock, sx }) => {
	const dispatch = useAppDispatch();
	const count = useAppSelector(selectProductCount(productId));

	const inc = () => {
		dispatch(productInc(productId));
	};

	const dec = () => {
		dispatch(productDec(productId));
	};

	return count > 0 ? (
		<Stack direction='row' spacing={1} sx={sx}>
			<IconButton onClick={dec}>
				<Remove />
			</IconButton>
			<Typography
				variant='h6'
				sx={{ display: 'flex', alignItems: 'center', p: 0, m: 0 }}>
				{count}
			</Typography>
			<IconButton disabled={count >= stock} onClick={inc}>
				<Add />
			</IconButton>
		</Stack>
	) : (
		<IconButton disabled={stock < 1} onClick={inc}>
			<AddShoppingCart />
		</IconButton>
	);
};

export default QuantityButton;
