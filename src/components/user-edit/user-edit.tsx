import { TUser } from '../../models/models';
import { FC } from 'react';
import { TMutationProps } from '../../store/types';
import { useTranslation } from 'react-i18next';
import { Grid, Stack } from '@mui/material';
import Box from '@mui/material/Box';
import { Controller } from 'react-hook-form';
import TextField from '@mui/material/TextField';
import LoadingButton from '@mui/lab/LoadingButton';

export type TUserEditFormValue = Pick<TUser, 'name' | 'about' | 'avatar'>;

type TUserEditProps = {
	user: TUser | null;
};

const UserEdit: FC<TUserEditProps & TMutationProps<TUserEditFormValue>> = ({
	user,
	onSubmit,
	control,
	errors,
	isValid,
	isSubmitting,
	isSubmitted,
}) => {
	const { t } = useTranslation();
	return (
		<>
			{user && (
				<Grid container spacing={2}>
					<Grid item xs={12} lg={6}>
						<img
							width={'100%'}
							src={user?.avatar || ''}
							alt='TODO'
							loading='lazy'
						/>
					</Grid>
					<Grid item xs={12} lg={6}>
						<Stack direction={'column'} spacing={2}>
							<Box
								component='form'
								onSubmit={onSubmit}
								noValidate
								sx={{ mt: 1 }}>
								<Controller
									name={'name'}
									control={control}
									render={({ field }) => (
										<TextField
											margin='normal'
											required
											fullWidth
											label={t('label.name')}
											autoComplete='name'
											error={!!errors.name?.message}
											helperText={errors.name?.message}
											{...field}
										/>
									)}
								/>
								<Controller
									name={'about'}
									control={control}
									render={({ field }) => (
										<TextField
											margin='normal'
											multiline
											rows={4}
											required
											fullWidth
											label={t('label.about')}
											autoComplete='about'
											error={!!errors.about?.message}
											helperText={errors.about?.message}
											{...field}
										/>
									)}
								/>
								<Controller
									name={'avatar'}
									control={control}
									render={({ field }) => (
										<TextField
											margin='normal'
											multiline
											rows={4}
											required
											fullWidth
											label={t('label.avatar')}
											autoComplete='avatar'
											error={!!errors.avatar?.message}
											helperText={errors.avatar?.message}
											{...field}
										/>
									)}
								/>
								<LoadingButton
									type='submit'
									disabled={isSubmitted && (!isValid || isSubmitting)}
									loading={isSubmitting}
									fullWidth
									variant='contained'
									sx={{ mt: 3, mb: 2 }}>
									{t('button.save')}
								</LoadingButton>
							</Box>
						</Stack>
					</Grid>
				</Grid>
			)}
		</>
	);
};

export default UserEdit;
