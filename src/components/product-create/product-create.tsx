import { TProduct } from '../../models/models';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Grid, Stack } from '@mui/material';
import Box from '@mui/material/Box';
import { Controller } from 'react-hook-form';
import TextField from '@mui/material/TextField';
import LoadingButton from '@mui/lab/LoadingButton';
import { TMutationProps } from '../../store/types';

export type TProductCreateFormValue = Pick<
	TProduct,
	'price' | 'discount' | 'description' | 'name' | 'pictures' | 'stock'
>;

const ProductCreate: FC<TMutationProps<TProductCreateFormValue>> = ({
	onSubmit,
	control,
	errors,
	isValid,
	isSubmitting,
	isSubmitted,
}) => {
	const { t } = useTranslation();
	return (
		<Grid container spacing={2}>
			<Grid item xs={12} lg={6}></Grid>
			<Grid item xs={12} lg={6}>
				<Stack direction={'column'} spacing={2}>
					<Box component='form' onSubmit={onSubmit} noValidate sx={{ mt: 1 }}>
						<Controller
							name={'name'}
							control={control}
							render={({ field }) => (
								<TextField
									margin='normal'
									required
									fullWidth
									label={t('label.name')}
									autoComplete='name'
									error={!!errors.name?.message}
									helperText={errors.name?.message}
									{...field}
								/>
							)}
						/>
						<Controller
							name={'description'}
							control={control}
							render={({ field }) => (
								<TextField
									margin='normal'
									multiline
									rows={4}
									required
									fullWidth
									label={t('label.description')}
									autoComplete='description'
									error={!!errors.description?.message}
									helperText={errors.description?.message}
									{...field}
								/>
							)}
						/>
						<Controller
							name={'pictures'}
							control={control}
							render={({ field }) => (
								<TextField
									margin='normal'
									multiline
									rows={4}
									required
									fullWidth
									label={t('label.pictures')}
									autoComplete='pictures'
									error={!!errors.pictures?.message}
									helperText={errors.pictures?.message}
									{...field}
								/>
							)}
						/>
						<Controller
							name={'price'}
							control={control}
							render={({ field }) => (
								<TextField
									margin='normal'
									required
									fullWidth
									label={t('label.price')}
									autoComplete='price'
									error={!!errors.price?.message}
									helperText={errors.price?.message}
									{...field}
								/>
							)}
						/>
						<Controller
							name={'discount'}
							control={control}
							render={({ field }) => (
								<TextField
									margin='normal'
									required
									fullWidth
									label={t('label.discount')}
									autoComplete='discount'
									error={!!errors.discount?.message}
									helperText={errors.discount?.message}
									{...field}
								/>
							)}
						/>
						<Controller
							name={'stock'}
							control={control}
							render={({ field }) => (
								<TextField
									margin='normal'
									required
									fullWidth
									label={t('label.stock')}
									autoComplete='stock'
									error={!!errors.stock?.message}
									helperText={errors.stock?.message}
									{...field}
								/>
							)}
						/>
						<LoadingButton
							type='submit'
							disabled={isSubmitted && (!isValid || isSubmitting)}
							loading={isSubmitting}
							fullWidth
							variant='contained'
							sx={{ mt: 3, mb: 2 }}>
							{t('button.save')}
						</LoadingButton>
					</Box>
				</Stack>
			</Grid>
		</Grid>
	);
};

export default ProductCreate;
