import { FC } from 'react';
import { AppBar, Toolbar } from '@mui/material';
import Logo from '../logo/logo';
import { TEST_ID } from '../../constants/constants';

const Footer: FC = () => {
	return (
		<AppBar
			color='primary'
			component='footer'
			position='static'
			sx={{ mt: 'auto' }}>
			<Toolbar>
				<Logo
					sx={{ width: 220, height: 55, m: 1 }}
					viewBox='0 0 220 55'
					data-testid={TEST_ID.FOOTER.FOOTER_LOGO_LINK}
				/>
			</Toolbar>
		</AppBar>
	);
};

export default Footer;
