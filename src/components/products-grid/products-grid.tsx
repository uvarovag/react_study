import { TProduct } from '../../models/models';
import { FC } from 'react';
import { Grid } from '@mui/material';
import ProductCardVertical from '../product-card-vertical/product-card-vertical';
import { withQuery } from '../../HOCs/with-query';

type TProductsGridProps = {
	products: TProduct[] | null;
};

const ProductsGrid: FC<TProductsGridProps> = ({ products }) => {
	return (
		<>
			{products && (
				<Grid container spacing={2}>
					{products.map((product) => {
						return (
							<Grid key={product.id} item xs={12} sm={6} md={4} lg={3}>
								<ProductCardVertical product={product} />
							</Grid>
						);
					})}
				</Grid>
			)}
		</>
	);
};

export default withQuery(ProductsGrid);
