import { TProduct } from '../../models/models';
import { IconButton, SxProps } from '@mui/material';
import { FC, useMemo } from 'react';
import { useSetLikeMutation } from '../../store/api/products-api';
import { useAppSelector } from '../../store/hooks';
import { selectUser } from '../../store/reducers/root-layout/selectors';
import { toast } from 'react-toastify';
import { getMessageFromError } from '../../utils/errorUtil';
import { Favorite, FavoriteBorder } from '@mui/icons-material';
import { useTranslation } from 'react-i18next';

type TFavoriteProps = {
	product: TProduct;
	sx?: SxProps;
};

const FavoriteButton: FC<TFavoriteProps> = ({ product, sx }) => {
	const { id: productId, likes } = product;
	const [setLikeMutation, result] = useSetLikeMutation();
	const user = useAppSelector(selectUser);
	const { t } = useTranslation();
	const like = useMemo(() => {
		return user ? likes.includes(user.id) : false;
	}, [likes, user]);
	const switchActive = () => {
		setLikeMutation({ productId, like: !like });
	};
	if (result.isError) {
		toast.error(
			getMessageFromError(result.error, t('message.unexpectedError'))
		);
		result.reset();
	}

	return (
		<IconButton
			disabled={result.isLoading}
			onClick={switchActive}
			sx={sx}
			color={like ? 'error' : 'inherit'}>
			{like ? <Favorite /> : <FavoriteBorder />}
		</IconButton>
	);
};

export default FavoriteButton;
