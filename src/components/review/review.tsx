import { TReview } from '../../models/models';
import { Stack, SxProps, Typography } from '@mui/material';
import { FC } from 'react';
import { Rating } from '@mui/lab';

type TPriceProps = {
	reviews: TReview[];
	sx?: SxProps;
};

const Review: FC<TPriceProps> = ({ reviews, sx }) => {
	return (
		<Stack spacing={3} sx={sx}>
			{reviews.map((review) => (
				<Stack key={review.id} spacing={1}>
					<Typography variant='h6'>{review.author.name}</Typography>
					<Rating value={review.rating} readOnly />
					<Typography variant='body1' sx={{ wordWrap: 'break-word' }}>
						{review.text}
					</Typography>
				</Stack>
			))}
		</Stack>
	);
};

export default Review;
