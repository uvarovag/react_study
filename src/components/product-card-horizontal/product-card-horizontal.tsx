import { TProduct } from '../../models/models';
import { FC } from 'react';
import { Link, useLocation } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import { Stack } from '@mui/material';
import Price from '../price/price';
import DiscountChip from '../discount-chip/discount-chip';
import Typography from '@mui/material/Typography';
import FavoriteButton from '../favorite-button/favorite-button';
import QuantityButton from '../quantity-button/quantity-button';
import { withSelect } from '../../HOCs/with-select';
import { useTranslation } from 'react-i18next';

export type TProductCardProps = {
	product: TProduct;
};

const ProductCardHorizontal: FC<TProductCardProps> = ({ product }) => {
	const NAVIGATE_PATH = `/product/${product.id}`;
	const { t } = useTranslation();
	const { pathname } = useLocation();

	return (
		<Card sx={{ display: 'flex', height: '15rem' }}>
			<Link to={NAVIGATE_PATH} state={{ from: pathname }}>
				<CardMedia
					component='img'
					alt={product.name}
					image={product.pictures}
					sx={{ width: '12rem', height: '15rem' }}
				/>
			</Link>
			<CardContent sx={{ width: '100%' }}>
				<Stack direction={'column'} spacing={2}>
					<Stack direction={'row'} spacing={2}>
						<Price price={product.price} discount={product.discount} />
						<DiscountChip price={product.price} discount={product.discount} />
					</Stack>
					<Typography variant='subtitle2' fontWeight='light'>
						{`${t('text.inStock')} ${product.stock} ${t('text.pc')}`}
					</Typography>
					<Typography variant='h6'>{product.name}</Typography>
					<Stack direction={'row'} spacing={2}>
						<FavoriteButton product={product} />
						<QuantityButton productId={product.id} stock={product.stock} />
					</Stack>
				</Stack>
			</CardContent>
		</Card>
	);
};

export default ProductCardHorizontal;
export const ProductCardHorizontalWithSelect = withSelect<TProductCardProps>(
	ProductCardHorizontal
);
