import '@testing-library/jest-dom';
import { screen, render } from '@testing-library/react';
import { TEST_ID } from '../../../constants/constants';
import { setupTestWrapper } from '../../../utils/setup-test-wrapper';
import { ThemeProvider } from '@mui/material';
import { theme } from '../../../app/theme';
import { Provider } from 'react-redux';
import store from '../../../store/store';
import Header from '../header';
import * as selectTokens from '../../../store/reducers/root-layout/selectors';
import * as selectCartCount from '../../../store/reducers/cart/selectors';

import { RouterProvider, createBrowserRouter } from 'react-router-dom';

// getBy / getAllBy - ищем синхронно
// queryBy / queryAllBy - ищем чего не должно быть
// findBy / findAllBy - ищем асинхронно

const router = createBrowserRouter([
	{
		path: '/',
		element: <Header />,
	},
]);

const testLogo = () => {
	test('Проверяем доступность Logo link', () => {
		expect(
			screen.getByTestId(TEST_ID.HEADER.HEADER_LOGO_LINK)
		).toBeInTheDocument();
	});
};

describe('Тест для компонента Header', () => {
	describe('Достунпость кнопок для неавторизированных пользователей', () => {
		beforeEach(() => {
			setupTestWrapper({ initialRouterPath: '/' });
		});

		testLogo();

		test('Проверяем доступность кнопки Login', () => {
			expect(
				screen.getByTestId(TEST_ID.HEADER.LOGIN_BUTTON)
			).toBeInTheDocument();
		});
	});

	describe('Достунпость кнопок для авторизированных пользователей', () => {
		beforeEach(() => {
			jest
				.spyOn(selectTokens, 'selectTokens')
				.mockImplementation(() => ({ accessToken: 'token', refreshToken: '' }));
			jest
				.spyOn(selectCartCount, 'selectCartCount')
				.mockImplementation(() => 10);
			render(
				<ThemeProvider theme={theme}>
					<Provider store={store}>
						<RouterProvider router={router} />
					</Provider>
				</ThemeProvider>
			);
		});

		testLogo();

		test('Проверяем доступность кнопок', () => {
			expect(
				screen.getByTestId(TEST_ID.HEADER.CART_BUTTON)
			).toBeInTheDocument();
			expect(
				screen.getByTestId(TEST_ID.HEADER.FAVORITE_BUTTON)
			).toBeInTheDocument();
			expect(
				screen.getByTestId(TEST_ID.HEADER.PROFILE_BUTTON)
			).toBeInTheDocument();
			expect(
				screen.getByTestId(TEST_ID.HEADER.LOGOUT_BUTTON)
			).toBeInTheDocument();
		});
	});
});
