import { FC } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { AppBar, Badge, Box, IconButton, Toolbar } from '@mui/material';
import HeaderSearch from '../header-search/header-search';
import {
	Face,
	FavoriteBorder,
	Login as LoginIcon,
	Logout as LogoutIcon,
	ShoppingCart,
} from '@mui/icons-material';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import { setTokens } from '../../store/reducers/root-layout/root-layout-slice';
import { selectTokens } from '../../store/reducers/root-layout/selectors';
import { selectCartCount } from '../../store/reducers/cart/selectors';
import Spacer from '../spacer/spacer';
import { TEST_ID } from '../../constants/constants';
import Logo from '../logo/logo';

const Header: FC = () => {
	const SHOW_SEARCH_PATHNAMES = ['/products', '/cart', '/favorites'];
	const LOGO_NAVIGATE_PATH = '/';
	const location = useLocation();
	const navigate = useNavigate();
	const dispatch = useAppDispatch();
	const { accessToken } = useAppSelector(selectTokens);
	const cartCount = useAppSelector(selectCartCount);

	const buttonsForAuthorized = (
		<>
			<IconButton
				component={Link}
				to={'/cart'}
				state={{ from: location.pathname }}
				size='large'
				color='inherit'
				data-testid={TEST_ID.HEADER.CART_BUTTON}>
				<Badge badgeContent={cartCount} color='error'>
					<ShoppingCart />
				</Badge>
			</IconButton>

			<IconButton
				component={Link}
				to={'/favorites'}
				state={{ from: location.pathname }}
				size='large'
				color='inherit'
				data-testid={TEST_ID.HEADER.FAVORITE_BUTTON}>
				<FavoriteBorder />
			</IconButton>

			<IconButton
				component={Link}
				to={'/profile'}
				state={{ from: location.pathname }}
				size='large'
				color='inherit'
				data-testid={TEST_ID.HEADER.PROFILE_BUTTON}>
				<Face />
			</IconButton>

			<IconButton
				size='large'
				color='inherit'
				onClick={() => {
					dispatch(setTokens({ accessToken: '', refreshToken: '' }));
					navigate('/signin', { state: { from: location.pathname } });
				}}
				data-testid={TEST_ID.HEADER.LOGOUT_BUTTON}>
				<LogoutIcon />
			</IconButton>
		</>
	);
	const buttonsForNonAuthorized = (
		<IconButton
			component={Link}
			to={'/signin'}
			state={{ from: location.pathname }}
			size='large'
			color='inherit'
			data-testid={TEST_ID.HEADER.LOGIN_BUTTON}>
			<LoginIcon />
		</IconButton>
	);

	const buttons = accessToken ? buttonsForAuthorized : buttonsForNonAuthorized;

	return (
		<AppBar color='primary' component='header' position='sticky'>
			<Toolbar>
				<Link
					to={LOGO_NAVIGATE_PATH}
					state={{ from: location.pathname }}
					data-testid={TEST_ID.HEADER.HEADER_LOGO_LINK}>
					<Logo sx={{ width: 220, height: 55, m: 1 }} viewBox='0 0 220 55' />
				</Link>
				<Spacer />
				{SHOW_SEARCH_PATHNAMES.includes(location.pathname) && (
					<HeaderSearch sx={{ display: { xs: 'none', sm: 'flex' } }} />
				)}
				<Box sx={{ ml: 2 }}>{buttons}</Box>
			</Toolbar>
		</AppBar>
	);
};

export default Header;
