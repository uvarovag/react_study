import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// eslint-disable-next-line import/no-named-as-default-member
const i18next = i18n.use(initReactI18next).init({
	resources: {
		en: {
			translation: {
				message: {
					unexpectedError: 'Unexpected error',
					signInSuccess: 'Welcome',
					nothingChanged: 'Nothing has changed',
					changesSaved: 'Changes saved',
				},
				button: {
					signIn: 'Sign In',
					signUp: 'Sign Up',
					selectAll: 'Select all',
					removeSelected: 'Remove selected',
					save: 'Save',
					cancel: 'Cancel',
					edit: 'Edit',
					create: 'Create',
				},
				label: {
					email: 'Email',
					group: 'Group',
					password: 'Password',
					name: 'Name',
					about: 'About',
					avatar: 'Avatar',
					description: 'Description',
					pictures: 'Picture',
					price: 'Price',
					discount: 'Discount',
					stock: 'Stock',
					text: 'Text',
				},
				title: {
					createReview: 'Create review',
				},
				text: {
					notFound: '404 Not Found',
					total: 'Total:',
					pc: 'pc:',
					inStock: 'In stock',
				},
			},
		},
	},
	lng: 'en',
	fallbackLng: 'en',

	interpolation: {
		escapeValue: false,
	},
});

export default i18next;
