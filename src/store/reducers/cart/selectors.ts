import { RootState } from '../../types';
import { cartSlice } from './cart-slice';

export const selectCart = (state: RootState) => {
	return state[cartSlice.name];
};

export const selectProductCount = (productId: string) => {
	return (state: RootState) => {
		return state[cartSlice.name][productId];
	};
};

export const selectCartCount = (state: RootState) => {
	const cart = selectCart(state);
	return Object.keys(cart).reduce((total, key) => total + cart[key], 0);
};
