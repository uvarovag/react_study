import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export const sliceName = 'cart';

export type TCart = {
	[key: string]: number;
};

const initialState: TCart = {};

export const cartSlice = createSlice({
	name: sliceName,
	initialState,
	reducers: {
		productInc: (store: TCart, action: PayloadAction<string>) => {
			store[action.payload] = (store[action.payload] || 0) + 1;
		},
		productDec: (store: TCart, action: PayloadAction<string>) => {
			store[action.payload] = (store[action.payload] || 0) - 1;
		},
		productsRemove: (store: TCart, action: PayloadAction<string[]>) => {
			action.payload.forEach((productId) => {
				productId in store && delete store[productId];
			});
		},
	},
});

export const { productInc, productDec, productsRemove } = cartSlice.actions;
export default cartSlice.reducer;
