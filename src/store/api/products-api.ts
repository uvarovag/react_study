import { createApi } from '@reduxjs/toolkit/query/react';
import { customBaseQuery } from './config';
import { TProduct, TReview } from '../../models/models';
import { TProductCreateFormValue } from '../../components/product-create/product-create';
import { TReviewCreateFormValue } from '../../components/review-create/review-create';

export type TProductsFilters = {
	page?: number;
	limit?: number;
	query?: string;
	productId?: string; // нужно для типизации setLike updateQueryData
};

export type TProductsResponse = {
	products: TProduct[];
	total: number;
};

type BE_TReview = Omit<TReview, 'id'> & {
	_id: TReview['id'];
};

type BE_TProduct = Omit<TProduct, 'id'> & {
	_id: TProduct['id'];
	reviews: BE_TReview[];
};

type BE_TProductsResponse = Omit<TProductsResponse, 'products'> & {
	products: BE_TProduct[];
};

function modifyReview(BE_Review: BE_TReview) {
	const { _id, ...rest } = BE_Review;
	return { id: _id, ...rest };
}

function modifyProduct(BE_Product: BE_TProduct) {
	const { _id, reviews, ...rest } = BE_Product;
	return { id: _id, reviews: reviews.map(modifyReview), ...rest };
}

export const productsApi = createApi({
	reducerPath: 'productsApi',
	baseQuery: customBaseQuery,
	tagTypes: ['Product', 'Products', 'CartProducts', 'FavoriteProducts'],
	endpoints: (builder) => ({
		fetchProducts: builder.query<TProductsResponse, TProductsFilters>({
			query: ({ page, limit, query }) => ({
				url: 'products',
				params: {
					page,
					limit,
					query,
				},
			}),
			providesTags: ['Products'],
			transformResponse: (BE_Data: BE_TProductsResponse) => {
				const { products, ...rest } = BE_Data;
				return { products: products.map(modifyProduct), ...rest };
			},
			serializeQueryArgs: ({ endpointName, queryArgs: { query } }) => {
				return endpointName + query;
			},
			merge: (currentCache, newValue, { arg: { page } }) => {
				if (page === 1) return;
				currentCache.products.push(...newValue.products);
			},
			forceRefetch({ currentArg, previousArg }) {
				return currentArg !== previousArg;
			},
		}),
		fetchCartProducts: builder.query<TProductsResponse, string | undefined>({
			query: (query) => ({
				url: 'products',
				params: {
					query,
					limit: 1000,
				},
			}),
			providesTags: ['CartProducts'],
			transformResponse: (BE_Data: BE_TProductsResponse) => {
				const { products, ...rest } = BE_Data;
				return { products: products.map(modifyProduct), ...rest };
			},
		}),
		fetchFavoriteProducts: builder.query<TProductsResponse, string | undefined>(
			{
				query: (query) => ({
					url: 'products',
					params: {
						query,
						limit: 1000,
					},
				}),
				providesTags: ['FavoriteProducts'],
				transformResponse: (BE_Data: BE_TProductsResponse) => {
					const { products, ...rest } = BE_Data;
					return { products: products.map(modifyProduct), ...rest };
				},
			}
		),
		fetchProduct: builder.query<TProduct, string>({
			query: (productId) => ({
				url: `products/${productId}`,
			}),
			providesTags: ['Product'],
			transformResponse: modifyProduct,
		}),
		createProduct: builder.mutation<TProduct, TProductCreateFormValue>({
			query: (productFormValue) => ({
				url: 'products',
				method: 'POST',
				body: productFormValue,
			}),
			invalidatesTags: ['CartProducts', 'FavoriteProducts'],
			transformResponse: modifyProduct,
		}),
		editProduct: builder.mutation<
			TProduct,
			{ productId: TProduct['id'] } & TProductCreateFormValue
		>({
			query: ({ productId, ...productCreateFormValue }) => ({
				url: `products/${productId}`,
				method: 'PATCH',
				body: productCreateFormValue,
			}),
			invalidatesTags: ['Product', 'CartProducts', 'FavoriteProducts'],
			transformResponse: modifyProduct,
		}),
		createReview: builder.mutation<
			TProduct,
			{ productId: TProduct['id'] } & TReviewCreateFormValue
		>({
			query: ({ productId, ...reviewCreateFormValue }) => ({
				url: `products/review/${productId}`,
				method: 'POST',
				body: reviewCreateFormValue,
			}),
			invalidatesTags: ['Product', 'CartProducts', 'FavoriteProducts'],
			transformResponse: modifyProduct,
		}),
		setLike: builder.mutation<TProduct, { productId: string; like: boolean }>({
			query: ({ productId, like }) => ({
				url: `products/likes/${productId}`,
				method: like ? 'PUT' : 'DELETE',
			}),
			invalidatesTags: ['Product', 'CartProducts', 'FavoriteProducts'],
			async onQueryStarted(_, { dispatch, queryFulfilled }) {
				try {
					const { data: updatedProduct } = await queryFulfilled;
					dispatch(
						productsApi.util.updateQueryData('fetchProducts', _, (draft) => {
							const productIndex = draft.products.findIndex(
								(product) => updatedProduct.id === product.id
							);
							productIndex !== -1 &&
								(draft.products[productIndex] = updatedProduct);
						})
					);
				} catch {}
			},
			transformResponse: modifyProduct,
		}),
	}),
});

export const {
	useFetchProductsQuery,
	useFetchCartProductsQuery,
	useFetchFavoriteProductsQuery,
	useFetchProductQuery,
	useCreateProductMutation,
	useEditProductMutation,
	useCreateReviewMutation,
	useSetLikeMutation,
} = productsApi;
