import { TUser } from '../../models/models';
import { createApi } from '@reduxjs/toolkit/dist/query/react';
import { customBaseQuery } from './config';
import { TSignUpFormValue } from '../../pages/sign-up-page/sign-up-page';
import { TSignInFormValue } from '../../pages/sign-in-page/sign-in-page';
import { BE_TUser, modifyUser } from './profile-api';

type TSignInResponse = {
	data: TUser;
	token: string;
};

type BE_TSignInResponse = Omit<TSignInResponse, 'data'> & {
	data: BE_TUser;
};

export const authApi = createApi({
	reducerPath: 'authApi',
	baseQuery: customBaseQuery,
	endpoints: (builder) => ({
		signUp: builder.mutation<TUser, TSignUpFormValue>({
			query: (signUpFormValues) => ({
				url: 'signup',
				method: 'POST',
				body: signUpFormValues,
			}),
		}),
		signIn: builder.mutation<TSignInResponse, TSignInFormValue>({
			query: (signInFormValues) => ({
				url: 'signin',
				method: 'POST',
				body: signInFormValues,
			}),
			transformResponse: (BE_Data: BE_TSignInResponse) => {
				const { data, ...rest } = BE_Data;

				return {
					data: modifyUser(data),
					...rest,
				};
			},
		}),
	}),
});

export const { useSignUpMutation, useSignInMutation } = authApi;
