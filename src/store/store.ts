import { configureStore } from '@reduxjs/toolkit';
import { persistedReducer } from './reducers/root-reducer';
import {
	persistStore,
	FLUSH,
	REHYDRATE,
	PAUSE,
	PERSIST,
	PURGE,
	REGISTER,
} from 'redux-persist';
import { authApi } from './api/auth-api';
import { productsApi } from './api/products-api';
import { profileApi } from './api/profile-api';

const store = configureStore({
	reducer: persistedReducer,
	devTools: process.env.NODE_ENV !== 'production',
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: {
				ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
			},
		})
			.concat(authApi.middleware)
			.concat(profileApi.middleware)
			.concat(productsApi.middleware),
});

export const persistor = persistStore(store);
export default store;
