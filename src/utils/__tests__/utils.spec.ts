import * as commonUtils from '../commonUtils';

describe('Тесты для утилит', () => {
	describe('Тесты для commonUtils', () => {
		describe(`Тесты для утилиты ${commonUtils.objectHasProperty.name}`, () => {
			const objectHasPropertySpy = jest.spyOn(commonUtils, 'objectHasProperty');
			const obj = { a: 0 };
			test(`${commonUtils.objectHasProperty.name} находит свойство в объекте`, () => {
				expect(commonUtils.objectHasProperty(obj, 'a')).toBeTruthy();
				expect(objectHasPropertySpy).toHaveBeenCalledTimes(1);
			});
			test(`${commonUtils.objectHasProperty.name} сообщает что свойства нет в объекте`, () => {
				expect(commonUtils.objectHasProperty(obj, 'b')).toBeFalsy();
				expect(objectHasPropertySpy).toHaveBeenCalledTimes(1);
			});
			test(`${commonUtils.objectHasProperty.name} обрабатывает кейс когда первый параметр не объект`, () => {
				const firstArgs = ['', 1, null, undefined];
				firstArgs.forEach((item) => {
					expect(commonUtils.objectHasProperty(item, 'b')).toBeFalsy();
				});
				expect(objectHasPropertySpy).toHaveBeenCalledTimes(firstArgs.length);
			});
		});
	});
});
