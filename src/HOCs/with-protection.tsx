import { ComponentType, FC } from 'react';
import { useLocation, Navigate } from 'react-router-dom';
import { useAppSelector } from '../store/hooks';
import { selectTokens } from '../store/reducers/root-layout/selectors';

export const withProtection = <P extends object>(
	WrappedComponent: ComponentType<P>
) => {
	const ReturnedComponent: FC<P> = (props) => {
		const location = useLocation();
		const { accessToken } = useAppSelector(selectTokens);
		if (!accessToken) {
			return <Navigate to='/signin' state={{ from: location.pathname }} />;
		}
		return <WrappedComponent {...props} />;
	};

	ReturnedComponent.displayName = `withProtection${ReturnedComponent.displayName}`;
	return ReturnedComponent;
};
