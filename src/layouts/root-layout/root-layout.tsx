import { Box, Container, CssBaseline } from '@mui/material';
import Header from '../../components/header/header';
import { Outlet } from 'react-router-dom';
import Footer from '../../components/footer/footer';

const RootLayout = () => {
	return (
		<Box
			sx={{
				display: 'flex',
				flexDirection: 'column',
				minHeight: '100vh',
			}}>
			<CssBaseline />
			<Header />
			<Container component='main' sx={{ mt: 8, mb: 2 }} maxWidth='lg'>
				<Outlet />
			</Container>
			<Footer />
		</Box>
	);
};

export default RootLayout;
