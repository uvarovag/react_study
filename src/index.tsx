import { createRoot } from 'react-dom/client';
import { ThemeProvider } from '@mui/material';
import { theme } from './app/theme';
import { PersistGate } from 'redux-persist/integration/react';
import store, { persistor } from './store/store';
import { Provider } from 'react-redux';
import App from './app/app';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './i18n/i18n';

const domNode = document.getElementById('root') as HTMLDivElement;
const root = createRoot(domNode);

root.render(
	<ThemeProvider theme={theme}>
		<PersistGate loading={null} persistor={persistor}>
			<Provider store={store}>
				<App />
				<ToastContainer theme={'colored'} position={'top-right'} />
			</Provider>
		</PersistGate>
	</ThemeProvider>
);
