import * as yup from 'yup';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useSignInMutation, useSignUpMutation } from '../../store/api/auth-api';
import { toast } from 'react-toastify';
import { getMessageFromError } from '../../utils/errorUtil';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import LoadingButton from '@mui/lab/LoadingButton';
import { Button, Stack } from '@mui/material';
import { useTranslation } from 'react-i18next';
import {
	setTokens,
	setUser,
} from '../../store/reducers/root-layout/root-layout-slice';
import { useAppDispatch } from '../../store/hooks';

export type TSignUpFormValue = {
	email: string;
	group: string;
	password: string;
};

export default function SignUpPage() {
	const NAVIGATE_PATH_SUCCESS = '/';
	const NAVIGATE_PATH_SIGN_IN = '/signin';

	const { t } = useTranslation();
	const navigate = useNavigate();
	const dispatch = useAppDispatch();
	const [signUpMutation] = useSignUpMutation();
	const [signInMutation] = useSignInMutation();

	const signUpFormSchema = yup.object({
		email: yup.string().email().required().strict(),
		group: yup.string().lowercase().required().strict(),
		password: yup.string().required().min(6).max(24),
	});

	const {
		control,
		handleSubmit,
		formState: { errors, isValid, isSubmitting, isSubmitted },
	} = useForm<TSignUpFormValue>({
		defaultValues: {
			email: '',
			group: '',
			password: '',
		},
		resolver: yupResolver(signUpFormSchema),
	});

	const submitHandler: SubmitHandler<TSignUpFormValue> = async (values) => {
		try {
			await signUpMutation(values);
			const response = await signInMutation({
				email: values.email,
				password: values.password,
			}).unwrap();
			dispatch(setTokens({ accessToken: response.token, refreshToken: '' }));
			dispatch(setUser(response.data));
			toast.success(t('message.signInSuccess'));
			navigate(NAVIGATE_PATH_SUCCESS);
		} catch (e) {
			toast.error(getMessageFromError(e, t('message.unexpectedError')));
		}
	};

	return (
		<Box
			sx={{
				marginY: 15,
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
			}}>
			<Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
				<LockOutlinedIcon />
			</Avatar>
			<Typography component='h1' variant='h5'>
				{t('button.signUp')}
			</Typography>
			<Box
				component='form'
				onSubmit={handleSubmit(submitHandler)}
				noValidate
				sx={{ mt: 1, maxWidth: '30rem' }}>
				<Controller
					name={'email'}
					control={control}
					render={({ field }) => (
						<TextField
							type='email'
							margin='normal'
							required
							fullWidth
							label={t('label.email')}
							autoComplete='email'
							error={!!errors.email?.message}
							helperText={errors.email?.message}
							{...field}
						/>
					)}
				/>
				<Controller
					name={'group'}
					control={control}
					render={({ field }) => (
						<TextField
							margin='normal'
							required
							fullWidth
							label={t('label.group')}
							autoComplete='group'
							error={!!errors.group?.message}
							helperText={errors.group?.message}
							{...field}
						/>
					)}
				/>
				<Controller
					name={'password'}
					control={control}
					render={({ field }) => (
						<TextField
							type='password'
							margin='normal'
							required
							fullWidth
							label={t('label.password')}
							autoComplete='password'
							error={!!errors.password?.message}
							helperText={errors.password?.message}
							{...field}
						/>
					)}
				/>
				<LoadingButton
					type='submit'
					disabled={isSubmitted && (!isValid || isSubmitting)}
					loading={isSubmitting}
					fullWidth
					variant='contained'
					sx={{ mt: 3, mb: 2 }}>
					{t('button.signUp')}
				</LoadingButton>
				<Stack direction='row' justifyContent='center' alignItems='center'>
					<Button
						component={RouterLink}
						to={NAVIGATE_PATH_SIGN_IN}
						state={{ from: location.pathname }}>
						{t('button.signIn')}
					</Button>
				</Stack>
			</Box>
		</Box>
	);
}
