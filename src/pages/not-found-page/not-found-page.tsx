import { Box, CssBaseline, Typography } from '@mui/material';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import { useTranslation } from 'react-i18next';

const NotFoundPage = () => {
	const { t } = useTranslation();
	return (
		<Box
			sx={{
				display: 'flex',
				flexDirection: 'column',
				minHeight: '100vh',
			}}>
			<CssBaseline />
			<Box sx={{ m: 'auto' }}>
				<Typography variant='h1'>{t('text.notFound')}</Typography>
				<NavBackButton />
			</Box>
		</Box>
	);
};

export default NotFoundPage;
