import { FC } from 'react';
import { Navigate } from 'react-router-dom';

const MainPage: FC = () => {
	return <Navigate to='/products'></Navigate>;
};

export default MainPage;
