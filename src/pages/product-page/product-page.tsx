import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useParams } from 'react-router-dom';
import { useFetchProductQuery } from '../../store/api/products-api';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import Product from '../../components/product/product';
import { getMessageFromError } from '../../utils/errorUtil';
import { withProtection } from '../../HOCs/with-protection';
import { Button, Stack } from '@mui/material';
import Spacer from '../../components/spacer/spacer';

const ProductPage: FC = () => {
	const { t } = useTranslation();
	const { productId } = useParams();
	const navigatePathEdit = (): string => `/product-edit/${productId}`;
	const { data, isLoading, isError, error, refetch } = useFetchProductQuery(
		productId || '',
		{ skip: !productId }
	);

	return (
		<>
			<Stack direction='row'>
				<NavBackButton sx={{ mb: 2 }} />
				<Spacer />
				<Button component={Link} to={navigatePathEdit()} sx={{ mb: 2 }}>
					{t('button.edit')}
				</Button>
			</Stack>
			<Product
				product={data || null}
				isLoading={isLoading}
				isError={isError}
				error={getMessageFromError(error, t('message.unexpectedError'))}
				refetch={refetch}
			/>
		</>
	);
};

export default withProtection(ProductPage);
