import { ChangeEvent, FC, useEffect, useMemo, useState } from 'react';
import { useFetchCartProductsQuery } from '../../store/api/products-api';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import {
	Button,
	Checkbox,
	FormControlLabel,
	Grid,
	Paper,
	Stack,
} from '@mui/material';
import ProductsStack from '../../components/products-stack/products-stack';
import Typography from '@mui/material/Typography';
import Spacer from '../../components/spacer/spacer';
import Price from '../../components/price/price';
import { withProtection } from '../../HOCs/with-protection';
import { useAppDispatch, useAppSelector } from '../../store/hooks';
import {
	selectCart,
	selectCartCount,
} from '../../store/reducers/cart/selectors';
import { TProduct } from '../../models/models';
import { selectHeaderSearchQuery } from '../../store/reducers/root-layout/selectors';
import { useDebounce } from '../../hooks/useDebouce';
import { productsRemove, TCart } from '../../store/reducers/cart/cart-slice';
import { TWithSelectProps } from '../../HOCs/with-select';
import { TProductCardProps } from '../../components/product-card-horizontal/product-card-horizontal';
import { useTranslation } from 'react-i18next';

type TProductsInCartData = {
	products: TProduct[];
	totalPrice: number;
	totalDiscount: number;
};

const getProductsInCartData = (
	products: TProduct[] | null,
	cart: TCart
): TProductsInCartData => {
	const result: TProductsInCartData = {
		products: [],
		totalPrice: 0,
		totalDiscount: 0,
	};
	(products || []).forEach((product) => {
		const productInCartCount = cart[product.id];
		if (product.id in cart && productInCartCount > 0) {
			result.products.push(product);
			result.totalPrice =
				result.totalPrice + product.price * productInCartCount;
			result.totalDiscount =
				result.totalDiscount + product.discount * productInCartCount;
		}
	});
	return result;
};

const CartPage: FC = () => {
	const { t } = useTranslation();
	const dispatch = useAppDispatch();
	const cart = useAppSelector(selectCart);
	const cartCount = useAppSelector(selectCartCount);
	const query = useAppSelector(selectHeaderSearchQuery);
	const debounceQuery = useDebounce<string>(query);
	const { data, isLoading, isError, refetch } = useFetchCartProductsQuery(
		debounceQuery || undefined
	);
	const { products, totalPrice, totalDiscount } = useMemo(
		() => getProductsInCartData(data?.products || null, cart),
		[cart, data]
	);
	const [selected, setSelected] = useState<string[]>([]);
	const [selectedAll, setSelectedAll] = useState<boolean>(false);

	useEffect(() => {
		setSelectedAll(products.length === selected.length);
	}, [selected, products]);

	const onSelectAll = (e: ChangeEvent<HTMLInputElement>) => {
		setSelected(e.target.checked ? products.map((product) => product.id) : []);
	};

	const onSelectItem: TWithSelectProps<TProductCardProps>['onSelect'] = (
		{ product },
		newValue
	) => {
		setSelected((prev) => {
			return !newValue && !prev.includes(product.id)
				? [...prev, product.id]
				: prev.filter((id) => id !== product.id);
		});
	};

	const onProductsRemove = () => {
		dispatch(productsRemove(selected));
	};

	return (
		<>
			<NavBackButton sx={{ mb: 2 }} />
			<Grid container spacing={2}>
				<Grid item xs={12} lg={8}>
					<Paper sx={{ mb: 2, display: 'flex' }}>
						<FormControlLabel
							control={
								<Checkbox checked={selectedAll} onChange={onSelectAll} />
							}
							label={t('button.selectAll')}
							sx={{ m: 1 }}
						/>
						<Spacer />
						{selected.length > 0 && (
							<Button onClick={onProductsRemove} sx={{ m: 1 }}>
								{t('button.removeSelected')}
							</Button>
						)}
					</Paper>
					<ProductsStack
						products={products}
						selected={selected}
						onSelect={onSelectItem}
						isLoading={isLoading}
						isError={isError}
						refetch={refetch}
					/>
				</Grid>
				<Grid item xs={12} lg={4}>
					<Paper sx={{ p: 2 }}>
						<Typography variant='subtitle2'>{t('text.total')}</Typography>
						<Stack direction={'row'} spacing={2} sx={{ display: 'flex' }}>
							<Typography variant='h6'>
								{`${cartCount} ${t('text.pc')}`}
							</Typography>
							<Spacer />
							<Price price={totalPrice} discount={totalDiscount} />
						</Stack>
					</Paper>
				</Grid>
			</Grid>
		</>
	);
};

export default withProtection(CartPage);
