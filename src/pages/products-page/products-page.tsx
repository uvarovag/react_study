import { FC, useCallback, useEffect, useState } from 'react';
import { useAppSelector } from '../../store/hooks';
import { selectHeaderSearchQuery } from '../../store/reducers/root-layout/selectors';
import { useDebounce } from '../../hooks/useDebouce';
import { useFetchProductsQuery } from '../../store/api/products-api';
import ProductsGrid from '../../components/products-grid/products-grid';
import { LoadMore } from '../../components/load-more/load-more';
import { withProtection } from '../../HOCs/with-protection';
import NavBackButton from '../../components/nav-back-button/nav-back-button';
import { getMessageFromError } from '../../utils/errorUtil';
import { useTranslation } from 'react-i18next';
import { Button, Stack } from '@mui/material';
import Spacer from '../../components/spacer/spacer';
import { Link } from 'react-router-dom';

const ProductsPage: FC = () => {
	const NAVIGATE_PATH_PRODUCT_CREATE = '/product-create';
	const FIRST_PAGE = 1;
	const LIMIT = 12;
	const { t } = useTranslation();
	const query = useAppSelector(selectHeaderSearchQuery);
	const debounceQuery = useDebounce<string>(query);
	const [page, setPage] = useState<number>(FIRST_PAGE);
	const { data, isLoading, isError, isFetching, error, refetch } =
		useFetchProductsQuery({
			page: page,
			limit: LIMIT,
			query: debounceQuery || undefined,
		});
	const isEndOfList = data && data.products.length >= data.total;
	const loadMore = useCallback(() => {
		setPage((prev) => prev + 1);
	}, []);

	useEffect(() => {
		setPage(FIRST_PAGE);
	}, [debounceQuery]);

	return (
		<>
			<Stack direction='row'>
				<NavBackButton sx={{ mb: 2 }} />
				<Spacer />
				<Button
					component={Link}
					to={NAVIGATE_PATH_PRODUCT_CREATE}
					sx={{ mb: 2 }}>
					{t('button.create')}
				</Button>
			</Stack>
			<ProductsGrid
				products={data?.products || null}
				isLoading={isLoading}
				isError={isError}
				error={getMessageFromError(error, t('message.unexpectedError'))}
				refetch={refetch}
			/>
			<LoadMore
				action={loadMore}
				isEndOfList={isEndOfList}
				isLoading={isFetching && page > 2} // TODO почему два???
			/>
		</>
	);
};

export default withProtection(ProductsPage);
