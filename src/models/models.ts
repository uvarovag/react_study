export type TUser = {
	id: string;
	name: string;
	group: string;
	email: string;
	about: string;
	avatar: string;
};

export type TProduct = {
	id: string;
	price: number;
	discount: number;
	description: string;
	name: string;
	pictures: string;
	stock: number;
	reviews: TReview[];
	likes: string[];
};

export type TReview = {
	id: string;
	product: string;
	text: string;
	rating: number;
	created_at: string;
	updated_at: string;
	author: TUser;
};
